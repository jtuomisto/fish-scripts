# Firefox profile helper script

source "$HOME/lib/iniparser.fish"

# This expects the default profile to have a "Default=1" key
function firefox_default_profile -d "Get the path for the default Firefox profile"
    set -l mozdirs "$HOME/.mozilla/firefox" \
                   "$HOME/.var/app/org.mozilla.firefox/.mozilla/firefox" \
                   "$HOME/.var/app/org.mozilla.Firefox/.mozilla/firefox"

    for d in $mozdirs
        set -l prof_file "$d/profiles.ini"

        if ! test -f "$prof_file"
            continue
        end

        ini_clear
        ini_parse "$prof_file"

        # Go through profiles and search for the default
        for i in (seq 0 99)
            set -l prof "Profile$i"

            if ini_has_key $prof Default
                set -l default (ini_get_value $prof Default)
                set -l relative (ini_get_value $prof IsRelative)
                set -l path (ini_get_value $prof Path)

                if test $default -eq 1
                    if test $relative -eq 1
                        echo "$d/$path"
                    else
                        echo "$path"
                    end

                    return 0
                end
            end
        end
    end

    return 1
end
