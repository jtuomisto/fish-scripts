# Common utility functions

# Check for 'freshness' of a file
function is_fresh -a time file
    if ! test -e "$file"
        return 1
    end

    set -l modified_epoch (stat "$file" --format=%Y)
    set -l current_epoch (date +%s)
    set -l delta (math $current_epoch - $modified_epoch)

    test $delta -lt $time
    return $status
end
