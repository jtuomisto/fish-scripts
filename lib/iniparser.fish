# Parses ini files (duh)


set _ini_prefix "_ini_parser_var"
set _ini_sep "_"
set _ini_dsep "_"


function _ini_varname -a s k
    set -l section (echo -n $s | string replace -ra '\s' $_ini_dsep)
    set -l key (echo -n $k | string replace -ra '\s' $_ini_dsep)
    echo -n $_ini_prefix$_ini_sep$section$_ini_sep$key
end


function ini_parse -a file -d "Parse ini file, this must be called before using any other ini functions"
    set -l section "DEFAULT"

    if ! test -f "$file"
        return 1
    end

    cat "$file" | while read -l line
        set -l trimmed (string trim $line)

        if test -z "$trimmed"
            continue
        end

        if grep -q '^\s*\[' (echo $trimmed | psub)
            set section (echo $trimmed | sed -re 's/\s*\[\s*([^]]+)\s*\]\s*/\1/')
        else
            set -l splitted (echo $trimmed | string split -m 1 =)
            set -l key (echo $splitted[1] | string trim)
            set -l value (echo $splitted[2] | string trim)
            set -g (_ini_varname $section $key) "$value"
        end
    end
end


function ini_clear -d "Clear ini variables, must be called before calling ini_parse again"
    for varname in (set -gn | grep "^$_ini_prefix")
        set -e $varname
    end
end


function ini_has_key -a section key -d "Check if the parsed ini file has key"
    set -q (_ini_varname $section $key)
    return $status
end


function ini_get_value -a section key -d "Get value of a key"
    set -l varname (_ini_varname $section $key)

    if set -q $varname
        echo $$varname
    else
        return 1
    end
end
