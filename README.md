Fish scripts
============

You may need these:

- fd
- jq
- notify-send
- rofi
- sqlite3
- udisks2
- xdg-open
- mimeopen
- wl-clipboard
- nmcli
- kdeconnect-cli
- sway
- Font Awesome 5

The scripts in `bin` assume that the `lib` folder is available at `$HOME/lib`
