#!/usr/bin/env fish

# Creates .desktop entries

# Desktop entry format:
#  [Desktop Entry]
#  Version=1.1
#  Type=Application
#  Name=App name
#  Comment=Comment about app
#  TryExec=executable
#  Exec=executable [args]
#  Icon=icon
#  Categories=categories

set applications "$HOME/.local/share/applications"

if test -n "$XDG_DATA_HOME"
    set applications "$XDG_DATA_HOME/applications"
end


function create_file
    # remove whitespace and slashes, convert to lowercase
    set -l name (echo $argv | sed -e 's#\s#_#g' -e 's#[/\\]#_#g' | tr "[:upper:]" "[:lower:]")
    set -l path "$applications/$name.desktop"

    if test -e "$path"
        # path exists, add random part to the filename
        set -l path (mktemp -q --suffix=.desktop -p "$applications" "$name-XXXXXX")
    else
        touch "$path"
    end

    echo $path
end


function write_keyvalue -a path key value
    if test -n "$value"
        printf "%s=%s\n" "$key" "$value" >> "$path"
    end
end


function write_entry_start -a path
    printf "[Desktop Entry]\nVersion=1.1\nType=Application\n" >> "$path"
end


function read_value -a prompt no_empty default
    set -l value

    while true
        read -L --shell -c "$default" -P "$prompt: " value

        if test \( "$no_empty" = "no_empty" \) -a \( -z "$value" \)
            printf "Value for '%s' cannot be empty\n" "$prompt" >&2
        else
            break
        end
    end

    echo $value
end


set entry_exec $argv[1]

if test ! -e "$entry_exec"
    printf "File does not exist: %s\n" "$entry_exec"
    exit 1
end

printf "Creating a desktop entry for: %s\n" "$entry_exec"

# get absolute path to executable
set entry_exec (realpath "$entry_exec")

# start reading info from user
set entry_name (read_value "Name" "no_empty")
set entry_exec (read_value "Exec" "no_empty" "$entry_exec")
set entry_comment (read_value "Comment (optional)")
set entry_icon (read_value "Icon (optional)")
set entry_categories (read_value "Categories (optional)")

if test -z "$entry_icon"
    set entry_icon "application-default-icon"
end

if test -z "$entry_categories"
    set entry_categories "Utility"
end

set filename (create_file "$entry_name")

write_entry_start "$filename"
write_keyvalue "$filename" "Name" "$entry_name"
write_keyvalue "$filename" "Comment" "$entry_comment"
write_keyvalue "$filename" "Exec" "$entry_exec"
write_keyvalue "$filename" "Icon" "$entry_icon"
write_keyvalue "$filename" "Categories" "$entry_categories"

printf "Created a desktop entry successfully: %s\n" "$filename"
